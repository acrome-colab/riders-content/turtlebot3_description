#ifndef GAZEBO_ROS_COLOR_SENSOR_HH
#define GAZEBO_ROS_COLOR_SENSOR_HH
 
#include <gazebo/common/Plugin.hh>
#include <ros/ros.h>
#include <gazebo/sensors/Sensor.hh>
#include <gazebo/sensors/CameraSensor.hh>
#include <gazebo/sensors/SensorTypes.hh>
#include <gazebo/physics/World.hh>
#include "gazebo/physics/PhysicsIface.hh"
#include <gazebo/plugins/CameraPlugin.hh>

#include "turtlebot3_description/ColorSensor.h"
#include <sensor_msgs/Image.h>


namespace gazebo
{
	class GAZEBO_VISIBLE ColorSensorPlugin : public SensorPlugin
	{
    // brief Constructor
		public: ColorSensorPlugin();

    // brief Destructor
		public: ~ColorSensorPlugin();

    // brief Load the plugin
    // param take in SDF root element
    public: virtual void Load(sensors::SensorPtr _parent, sdf::ElementPtr _sdf);

    // brief Update the controller
    public: virtual void OnNewFrame(const unsigned char *_image,
      unsigned int _width, unsigned int _height,
        unsigned int _depth, const std::string &_format);

    // Ros Service Callback
    protected: virtual bool ColorCallback(turtlebot3_description::ColorSensor::Request &req,
    turtlebot3_description::ColorSensor::Response &res);

    // Function to scale image
    protected: virtual void ScaleImage(const char* image, int height, int width, uint8_t scale_rate);

    // Ros variables
    protected: std::unique_ptr<ros::NodeHandle> rosNode;

    protected: ros::Publisher color_sensor_publisher;
    protected: ros::ServiceServer color_service;

    protected: sensor_msgs::Image image_msg_;
    protected: std::string topic_name, service_name;


    // Image args
    protected: unsigned int width, height;
    protected: uint8_t scale_rate;
    protected: char *image;
    protected: char *topic_image;

    protected: std::string format, encoding;
    protected: std::string robot_namespace_, frame_name_;
    protected: uint32_t sequence;

    protected: sensors::SensorPtr sensor;
    protected: sensors::CameraSensorPtr parentSensor;
    protected: rendering::CameraPtr camera;
    protected: physics::WorldPtr world;

    protected: event::ConnectionPtr newFrameConnection;
	};
}
#endif
