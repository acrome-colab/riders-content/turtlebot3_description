#ifndef GAZEBO_ROS_LASER_SENSOR_HH
#define GAZEBO_ROS_LASER_SENSOR_HH

#include <gazebo/common/Plugin.hh>
#include <ros/ros.h>
#include <gazebo/sensors/Sensor.hh>
#include <gazebo/sensors/RaySensor.hh>
#include <gazebo/sensors/SensorTypes.hh>
#include <gazebo/physics/PhysicsTypes.hh>
#include <gazebo/physics/MultiRayShape.hh>
#include <gazebo/physics/World.hh>
#include <gazebo/physics/PhysicsIface.hh>

#include "turtlebot3_description/LaserSensor.h"
#include <std_msgs/Float64.h>


namespace gazebo
{
  class GAZEBO_VISIBLE LaserSensorPlugin : public SensorPlugin
  {
    // brief Constructor
    public: LaserSensorPlugin();

    // brief Destructor
    public: ~LaserSensorPlugin();

    // brief Load the plugin
    // param take in SDF root element
    public: virtual void Load(sensors::SensorPtr _parent, sdf::ElementPtr _sdf);

    public: virtual void OnNewLaserScans();

    public: virtual bool RangeCallback(turtlebot3_description::LaserSensor::Request &req,
                                       turtlebot3_description::LaserSensor::Response &res);
    // Ros variables
    protected: std::unique_ptr<ros::NodeHandle> rosNode;

    protected: ros::Publisher laser_sensor_publisher;
    protected: ros::ServiceServer laser_service;
    protected: std_msgs::Float64 range_msg;

    protected: std::string topic_name, service_name;

    // Sensor Variables
    protected: std::string robot_namespace_, frame_name_;
    protected: double distance;

    protected: uint32_t sequence;
    protected: sensors::RaySensorPtr parentSensor;
    protected: sensors::SensorPtr sensor;
    protected: physics::WorldPtr world;
    protected: std::vector<double> rangeVector;

    protected: event::ConnectionPtr newLaserScansConnection;
  };
}
#endif
